import collections
import numpy as np
import random
from snowballstemmer import TurkishStemmer

turkStem = TurkishStemmer()


class ReadFile(object):
    # Class Attribute
    species = "HMM Model"
    wordset = set()

    # Initializer / Instance Attributes
    def __init__(self, path, train_indexes, test_indexes):
        file = open(path, "r+")
        sentences = file.read().splitlines()
        random.shuffle(sentences)
        file.close()
        self.train_data = sentences[train_indexes[0]:train_indexes[1]]
        self.test_data = sentences[test_indexes[0]:test_indexes[1]]
        self.smoot = 0.001
    def WordSplitter(self, wordlist):
        tags = wordlist.copy()
        tag_dict = dict()
        initial_dict = dict()
        for i, eleman in enumerate(wordlist):
            wordlist[i] = eleman.split()
            tags[i] = wordlist[i].copy()
            for k, item in enumerate(wordlist[i]):
                word, tag = item.split("/")
                word = word.lower()
                #word = turkStem.stemWord(word)
                self.wordset.add(word)
                tag_dict.get(tag, tag_dict.setdefault(tag, [])).append(word)
                wordlist[i][k] = word
                tags[i][k] = tag
            initial_dict.setdefault(tags[i][0], 0)
            initial_dict[tags[i][0]] += 1

        denominator = np.sum(list(initial_dict.values())) + (self.smoot*initial_dict.keys().__len__())
        for key, value in initial_dict.items():
            initial_dict[key] = np.log2((value + (self.smoot))/ denominator)
        initial_dict["LAPLACE"] = np.log2(self.smoot / denominator)

        return wordlist, tags, tag_dict, initial_dict

    def CreateTransition(self, tag_list, tag_vocab):
        transition_list = list()

        for sentence in tag_list:
            for i, j in zip(sentence[0::1], sentence[1::1]):
                transition_list.append(i + " " + j)

        transition_dict = collections.Counter(transition_list)

        total = np.sum(list(transition_dict.values()))
        vocab_size = transition_dict.keys().__len__()*self.smoot
        denominator = total + vocab_size
        transition_matrix = np.zeros(shape=(len(tag_vocab), len(tag_vocab)))
        for index, key in enumerate(tag_vocab):
            for index_2, key_2 in enumerate(tag_vocab):
                transition_matrix[index, index_2] = np.log2((transition_dict.get(key + " " + key_2,
                                                                                        0) + self.smoot) / (denominator))

        return transition_matrix

    def CreateEmission(self, tag_dict):

        emission_dict = {}

        for key, value in tag_dict.items():
            emission_dict[key] = dict(collections.Counter(value))


        for key, value in tag_dict.items():
            emission_dict[key] = dict(collections.Counter(value))
            total = np.sum(list(emission_dict[key].values()))
            vocab_size = emission_dict[key].keys().__len__()*self.smoot
            denominator = total + (self.smoot*vocab_size)
            for k, v in emission_dict[key].items():


                emission_dict[key][k] = np.log2((v + self.smoot) / (denominator))

            emission_dict[key]["LAPLACE"] = np.log2(self.smoot / (denominator))

        return emission_dict

    def CreateTrain(self):
        word_list, tag_list, tag_dict, initial_dict = self.WordSplitter(self.train_data)

        emission_dict = self.CreateEmission(tag_dict)

        transition_matrix = self.CreateTransition(tag_list, list(emission_dict.keys()))

        return {"emission_dict": emission_dict,
                "initial_dict": initial_dict, "transition_matrix": transition_matrix}

    def CreateTest(self):
        word_list, tag_list, tag_dict, initial_dict = self.WordSplitter(self.test_data)

        return {"words": word_list, "tags": tag_list}


class Viterbi(object):
    # Class Attribute
    species = "Viterbi"

    def __init__(self, train, test):
        self.emission = train["emission_dict"]
        self.transition_matrix = train["transition_matrix"]
        self.test_words = test["words"]
        self.test_tags = test["tags"]
        self.initial_dict = train["initial_dict"]

        self.viterbiAlgorithm_new()


    def viterbiAlgorithm_new(self):
        total = 0
        result_test = 0
        kaggle_list = []
        f = open("results_with_accuracy.txt", "w+")
        f2 = open("results.txt", "w+")
        for index, test in enumerate(self.test_words):
            first = False
            taglist = list(self.emission.keys())
            matrix_dict = np.zeros(shape=(len(taglist), len(test)), dtype={'names': ('coming_tag', 'total_prob',),
                                                                           'formats': ('U10', '<f8')})
            old_probs = np.zeros(shape=(len(taglist)))
            for testindex, i in enumerate(test):
                if first == False:
                    first = True
                    for indis, tag in enumerate(taglist):
                        emission_prob = self.emission[tag].get(i, self.emission[tag]["LAPLACE"])
                        initial_transition_prob = self.initial_dict.get(tag, self.initial_dict["LAPLACE"])
                        matrix_dict[indis, testindex] = (
                        tag, initial_transition_prob + emission_prob + old_probs[indis])

                else:
                    for indis, tag in enumerate(taglist):
                        temp_max_array = np.zeros(shape=(len(taglist)))
                        emission_prob = self.emission[tag].get(i, self.emission[tag]["LAPLACE"])
                        for indis2, tag2 in enumerate(taglist):
                            temp_max_array[indis2] = self.transition_matrix[indis2, indis] + emission_prob + old_probs[
                                indis2]

                        matrix_dict[indis, testindex] = (
                        taglist[int(np.argmax(temp_max_array))], np.max(temp_max_array))

                old_probs = [x[1] for x in matrix_dict[:, testindex]]

            shapex = matrix_dict.shape[1] - 1

            item = max(matrix_dict[:, shapex], key=lambda item: item[1])
            actual_index = list(item == matrix_dict[:, shapex]).index(True)

            prevtagindex = taglist.index(item[0])
            newlist = []
            newlist.append(taglist[actual_index])
            shapex -= 1
            while True:
                item = matrix_dict[prevtagindex, shapex]
                actual_index = prevtagindex
                newlist.append(taglist[actual_index])
                prevtagindex = taglist.index(item[0])
                shapex -= 1
                if shapex == -1:
                    break

            newlist.reverse()
            kaggle_list += newlist
            total += newlist.__len__()
            res = (np.array(self.test_tags[index]) == np.array(newlist)).sum()
            result_test += res
            f.write("[TEST SENTENCE] --> " + " ".join(test) + "\n" + "[TEST TAGS] ------> " + " ".join(self.test_tags[
                                                                                                           index]) + "\n" + "[PREDICTED TAGS] -> " + " ".join(
                newlist) + "\n[TEST ACCURACY] --> %" + str(res * 100 / newlist.__len__()) + "\n\n")
            for count, word in enumerate(self.test_words[index]):
                if count != len(self.test_words[index]) - 1:
                    f2.write(word + "/" + newlist[count] + " ")
                else:
                    f2.write(word + "/" + newlist[count] + "\n")

        print("Accuracy Of All Test Data :", (result_test / total) * 100)
        f.close()
        f2.close()
        self.kaggleWriter(kaggle_list)

    def kaggleWriter(self,predicted):
        import csv
        with open('kaggle_result.csv', mode='w') as kaggle_file:
            kaggle_writer = csv.writer(kaggle_file, delimiter=',')
            kaggle_writer.writerow(['Id', 'Category'])
            for i , word in enumerate(predicted):
                kaggle_writer.writerow([i+1,word])