import Models
from settings import *
import time

start = time.time()
HMM_model = Models.ReadFile(DATASET_PATH, TRAIN_DATA_INDEXES, TEST_DATA_INDEXES)

train_data= HMM_model.CreateTrain()

test_data = HMM_model.CreateTest()

Models.Viterbi(train_data,test_data)

print("Program Run Time:",time.time()-start)

